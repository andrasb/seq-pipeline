
import time


class timer():
    def __init__(self):
        # dicts for organizing timer results
        self.t0      = {}   # starting epoch
        self.t1      = {}   # ending epoch
        
    def start(self,name):
        self.t0[name] = time.time()

    def end(self,name,logger=None):
        t1 = time.time()
        if name not in self.t0:
            print("ERROR: no matching timer start call detected for %s" % name)
            exit(1)
        if logger is not None:
            logger.info("[timer]: %s = %f (secs)" % (name,t1-self.t0[name]))
        else:
            print("[timer]: %s = %f (secs)" % (name,t1-self.t0[name]))
