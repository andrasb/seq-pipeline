#!/usr/bin/env python3
import logging
import configparser
import argparse
import coloredlogs
import os
import multiprocessing
import mytimer
import urllib.request
import humanize
import subprocess
import sys

#---
# Simple error wrapper to include exit
def ERROR(output):
    logging.error("\nERROR: "+output+"\n")
    exit(1)

#---
# Main worker class to help facilitate download of NCBI files and sequencing analysis
class seq_analysis_tool(object):

    #---
    # class init
    def __init__(self):
        coloredlogs.install(level='INFO',fmt="%(message)s")
        logging.info("\n" + '--' * 20)
        logging.info("Sequencing Analysis Tool");
        logging.info('--' * 20)

        # parse command-line arguments
        parser = argparse.ArgumentParser()
        parser.add_argument("--configFile",help="runtime config file name (default = config)",type=str,default="config")
        parser.add_argument("--threads",help="override maxThreads option from config file",type=int)
        parser.add_argument("--no-cache",dest='cache',help="override and invalidate local file cache",action="store_false")
        parser.add_argument("--do",help="run SRA downloads (SRA), fastq decompression (FQ), or both (default = both)",type=str,default="both")
        parser.add_argument("--cd",help="override sraFileName (runList) from config file",type=str,default="")
        parser.set_defaults(cache=True)
        args = parser.parse_args()

        self.configFileName=args.configFile

        if not args.cache:
            self.overrideCache = True
        else:
            self.overrideCache = False

        if args.threads:
            self.overrideThreads = args.threads
        else:
            self.overrideThreads = 0

        if args.do == "SRA":
            self.doing  = "Downloading SRA files"
            self.doFlag = 0
        elif args.do == "FQ":
            self.doing  = "Decompressing to fastq"
            self.doFlag = 1
        else:
            self.doing  = "Downloading SRAs and Decompressing to fastq"
            self.doFlag = 2

        if args.cd != "":
            self.overrideList = 1
            self.runList = args.cd

        # init a simple timer
        self.atimer = mytimer.timer()

        self.runConfig = None
        self.maxDownloadThreads = 1
        self.maxThreadsAllowed  = 68

        self.fileList = []
        self.pairedList = []
        self.pairedListPath = './currPairedList'

    #--
    # Check if [cached] filepath exists and filesize is > 0
    def checkFilePath(self,path):
        message = " checking if " + path + " is available locally..."

        if not os.path.isfile(path):
            logging.info("[-caching]:" + message + "no")
            return False
        else:
            fileSize=os.path.getsize(path)
            if fileSize:
                logging.info("[-caching]:" + message + "yes (%s)" % humanize.naturalsize(fileSize))
                return True
            else:
                logging.warn("zero file size detected")
                return False


    #---
    # parse runtime configuration file
    def parseConfig(self):

        logging.warn("\nReading runtime config information from file -> %s \n" % self.configFileName)

        if os.path.isfile(self.configFileName):
            self.runConfig = configparser.ConfigParser(inline_comment_prefixes='#',interpolation=configparser.ExtendedInterpolation())
            try:
                self.runConfig.read(self.configFileName)
            except configparser.DuplicateSectionError:
                ERROR("Duplicate section detected in config file: %s" % self.configFileName)
            except:
                ERROR("Unable to parse runtime config file: %s" % self.configFileName)

            # read global settings
            try:
                self.ncbiUrl         = self.runConfig.get('global','ncbi_url')
                self.sraCacheDir     = self.runConfig.get('global','sraCacheDir')
                self.fastqCacheDir   = self.runConfig.get('global','fastqCacheDir')
                
                if self.overrideCache:
                    logging.warn("[note: caching disabled via override with command-line arg]")
                    self.enableCaching = False
                else:
                    self.enableCaching = self.runConfig.getboolean('global','cacheDownload',fallback=False)

                if self.overrideList:
                    self.sraFileName = self.runList
                else:
                    self.sraFileName = self.runConfig.get('global','sraFileName')

                self.pairedListPath = os.path.dirname(self.sraFileName)+"/currPairedList"

                if self.overrideThreads > 0:
                    logging.warn("[note: maxThreads overridden with command-line option]")
                    self.downloadThreads = self.overrideThreads
                else:
                    self.downloadThreads = int(self.runConfig.get('global','maxDownloadThreads'))
                
                self.sraPath         = self.runConfig.get('global','sratoolsDir')
                self.fastqDumpOpts   = self.runConfig.get('global','fastqDumpOpts')

                if os.path.isfile(self.sraFileName):
                    with open(self.sraFileName,'r') as filehandle:
                        contents = filehandle.readlines()
                        filehandle.close()
                    self.fileList = [x.strip() for x in contents]
                else:
                    ERROR("Unable to read SRA input file = %s" % self.sraFileName)

            except:
                ERROR("Unable to parse global runtime settings")

            logging.info("")
            logging.info("--> Jobs to be done           = %s" % self.doing)
            logging.info("--> NCBU download url         = %s" % self.ncbiUrl)
            logging.info("--> SRA input filename        = %s" % self.sraFileName)
            logging.info("--> Enable SRA caching        = %s" % self.enableCaching)
            logging.info("--> Local SRA caching dir     = %s" % self.sraCacheDir)
            logging.info("--> Local fastq caching dir   = %s" % self.fastqCacheDir)
            logging.info("--> Max download threads      = %i" % self.downloadThreads)
            logging.info("--> Path to SRA Toolkit Utils = %s" % self.sraPath)
            logging.info("--> fastq-dump options        = %s" % self.fastqDumpOpts)

            assert(self.downloadThreads > 0)

        else:
            ERROR("--> unable to access config file")

    #---
    # download SRA files from a list contained in text file
    def downloadSRA_Files(self):
        logging.warn("\nDownload SRA Files:")
        logging.info("Checking on SRA files specified in -> %s" % self.sraFileName)

        # create sra cache directory
        if not os.path.isdir(self.sraCacheDir):
            logging.warn("\nCreating cache directory for local downloads -> %s" % self.sraCacheDir)
            try:
                os.makedirs(self.sraCacheDir,exist_ok=True)
            except:
                ERROR("Unable to create directory -> %s" % self.sraCacheDir)

        self.sraFiles = []

        if self.enableCaching:
            # check if files already exist locally (in cache mode)
            for file in self.fileList:
                localPath=self.sraCacheDir + '/' + file + '.sra'

                if not self.checkFilePath(localPath):
                    self.sraFiles.append(file)
        else:
            self.sraFiles = self.fileList

        for entry in self.sraFiles:
            logging.debug("    --> %s" % entry)

        # downlad SRA files (in parallel)

        assert(self.downloadThreads > 0)
        assert(self.downloadThreads <= self.maxThreadsAllowed )

        pool = multiprocessing.Pool(self.downloadThreads)

        self.atimer.start("Download SRA Files")
        pool.map(self.downloadSRA_File,self.sraFiles)
        self.atimer.end("Download SRA Files", logging)


    #---
    # download specific SRA file from NCBI web site
    def downloadSRA_File_orig(self,name):
        # NCBI organizes download path based on hierarchy, e.g.
        # SRR2296813 -> SRR/SRR229/SRR2296813/SRR2296813.sra

        logging.info("    --> initiating download of SRA file = %s" % name)

        assert(len(name) == 10)

        url = name[:3] + '/' + name[:6] + '/' + name + '/' + name + '.sra'
        url = self.ncbiUrl + url

        logging.debug('          --> url = %s' %  url)
        localFile=self.sraCacheDir + '/' + name + '.sra'
        urllib.request.urlretrieve(url,filename=localFile)

        logging.warn("    <-- completed download of SRA file  = %s" % name)

    def downloadSRA_File(self,name):

        logging.info("    --> initiating download of SRA file = %s" % name)

        assert(len(name) == 10)

        cmd = [self.sraPath + "/" + 'prefetch']
        cmd.append(name)
        cmd.append("-O")
        cmd.append(self.sraCacheDir)
        logging.debug("prefetch command -> %s\n" % cmd)

        p = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        stdout,stderr = p.communicate()
        logging.debug(stdout.decode())
        if stderr.decode(sys.getfilesystemencoding()) is not '':
            logging.info(stderr.decode(sys.getfilesystemencoding()))


        localPath=self.sraCacheDir + '/' + name + '.sra'
        if not os.path.isfile(localPath):
            ERROR("expected sra file not downloaded -> %s" % localPath)
            return 1
        logging.warn("    <-- completed download of SRA file  = %s" % name)
        return 0

    #---
    # convert SRA files with fastq-dump 
    def convertSRA_Files(self):
        logging.warn("\nConverting SRA Files to fastq format:")
        logging.info("Checking on fastq files specified in -> %s" % self.sraFileName)

        # cache directory should already exist
        #assert os.path.isdir(self.fastqCacheDir)
        # create fastq cache directory
        if not os.path.isdir(self.fastqCacheDir):
            logging.warn("\nCreating cache directory for fastq files -> %s" % self.fastqCacheDir)
            try:
                os.makedirs(self.fastqCacheDir,exist_ok=True)
            except:
                ERROR("Unable to create directory -> %s" % self.fastqCacheDir)

        if os.path.isfile(self.pairedListPath):
            with open(self.pairedListPath,'r') as filehandle:
                pairedContents = filehandle.readlines()
                filehandle.close()
            self.pairedList = [x.strip() for x in pairedContents]
        else:
            logging.warn("[note: did not find a currPairedList file -> %s]\n[ ^----If you are only running single ends this is fine. ]" % self.pairedListPath)

        fastqFiles = []

        if self.enableCaching:
            # check if files already exist locally (in cache mode)
            for file in self.fileList:

                localPath=self.fastqCacheDir + '/' + file + '_1.fastq'
                if not self.checkFilePath(localPath):
                    fastqFiles.append(file)
                    continue

                if file in self.pairedList:
                    localPath=self.fastqCacheDir + '/' + file + '_2.fastq'
                    if not self.checkFilePath(localPath):
                        fastqFiles.append(file)
        else:
            fastqFiles = self.fileList

        for entry in fastqFiles:
            logging.debug("    --> %s" % entry)

        # convert SRA files (in parallel)

        assert(self.downloadThreads > 0)
        assert(self.downloadThreads <= self.maxThreadsAllowed )

        pool = multiprocessing.Pool(self.downloadThreads)

        self.atimer.start("Convert SRA Files (%s threads)" % self.downloadThreads)
        workerStatus = pool.map(self.convertSRA_File,fastqFiles)
        if 1 in workerStatus:
            ERROR("One or more fastq conversions failed")
        self.atimer.end("Convert SRA Files (%s threads)" % self.downloadThreads, logging)

    #---
    # convert individual SRA file
    def convertSRA_File(self,name):

        fastqBinary = 'fastq-dump'

        logging.info("    --> initiating fastq conversion of SRA file = %s (binary = %s)" % (name,fastqBinary))

        cmd = [self.sraPath + "/" + fastqBinary]
        cmd.append("--outdir")
        cmd.append(self.fastqCacheDir)
        cmd = cmd + self.fastqDumpOpts.split()
        cmd.append(self.sraCacheDir + '/' + name + '.sra' )
        logging.debug("fastq binary conversion command -> %s\n" % cmd)

        p = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        stdout,stderr = p.communicate()
        logging.debug(stdout.decode())
        if stderr.decode(sys.getfilesystemencoding()) is not '':
            logging.info(stderr.decode(sys.getfilesystemencoding()))


        localPath=self.fastqCacheDir + '/' + name + '_1.fastq'
        if not os.path.isfile(localPath):
            ERROR("expected fastq file not created-> %s" % localPath)
            return 1

        if name in self.pairedList:
            localPath=self.fastqCacheDir + '/' + name + '_2.fastq'
            if not os.path.isfile(localPath):
                ERROR("expected fastq file not created-> %s" % localPath)
                return 1

        logging.warn("    <-- completed fastq conversion of SRA file  = %s" % name)
        return 0

def main():
    tool = seq_analysis_tool()
    
    tool.parseConfig()

    if tool.doFlag == 0:
        tool.downloadSRA_Files()
    elif tool.doFlag == 1:
        tool.convertSRA_Files()
    else:
        tool.downloadSRA_Files()
        tool.convertSRA_Files()


if __name__ == '__main__':
    main()